#ifndef __LIST_H__
#define __LIST_H__

#include <stddef.h>
#include <stdint.h>

struct ptr_list {
	uintptr_t *arr;
	size_t sz;
	size_t cap;
};

void ptr_list_init(struct ptr_list *list);
void ptr_list_free(struct ptr_list *list);
void ptr_list_reserve(struct ptr_list *list, size_t new_cap);
void ptr_list_push(struct ptr_list *list, void *ptr);

#endif // __LIST_H__
