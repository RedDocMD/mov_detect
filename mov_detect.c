#include "coloured.h"
#include "dr_api.h"
#include "list.h"

static void run_on_exit();
static dr_emit_flags_t on_basic_block(void *ctx, void *tag, instrlist_t *bb,
				      bool for_trace, bool translating);

struct ptr_list load_instrs;
void *bb_lock;

DR_EXPORT void dr_client_main(client_id_t id, int argc, const char **argv)
{
	dr_register_exit_event(run_on_exit);
	dr_register_bb_event(on_basic_block);
	bb_lock = dr_mutex_create();
	ptr_list_init(&load_instrs);
}

static void run_on_exit()
{
	struct col_str *msg = col_str_from("Hello, World!");
	msg = with_color(msg, COL_RED);
	msg = with_effect(msg, EF_BOLD, 0);
	msg = with_effect(msg, EF_UNDERLINE, 1);
	char *raw = col_str_raw(msg);
	printf("%s\nAnd this is plain.\n", raw);
	printf("%lu load instructions detected\n", load_instrs.sz);

	free(raw);
	free(msg);
	dr_mutex_destroy(bb_lock);
	ptr_list_free(&load_instrs);
}

static dr_emit_flags_t on_basic_block(void *ctx, void *tag, instrlist_t *bb,
				      bool for_trace, bool translating)
{
	instr_t *instr;
	int opcode;

	dr_mutex_lock(bb_lock);
	for (instr = instrlist_first(bb); instr;
	     instr = instr_get_next(instr)) {
		opcode = instr_get_opcode(instr);
		if (opcode == OP_mov_ld)
			ptr_list_push(&load_instrs, instr);
	}
	dr_mutex_unlock(bb_lock);

	return DR_EMIT_DEFAULT;
}
