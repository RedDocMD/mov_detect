#ifndef __COLOURED_H__
#define __COLOURED_H__

enum effect_code {
	EF_NONE = -1,
	EF_RESET = 0,
	EF_BOLD = 1,
	EF_ITALIC = 3,
	EF_UNDERLINE = 4,
	EF_BLINK = 5,
	EF_REVERSED = 7,
};

enum colour_code {
	COL_DEFAULT = 0,
	COL_BLACK = 30,
	COL_RED = 31,
	COL_GREEN = 32,
	COL_YELLOW = 33,
	COL_BLUE = 34,
	COL_MAGENTA = 35,
	COL_CYAN = 36,
	WHITE = 37,
};

#define BRIGHT(col) ((col) + 60)
#define BG(col) ((col) + 10)

struct col_str {
	char *buf;
	enum colour_code col;
	enum effect_code eff[2];
};

// Creates a col_str by allocating memory for it
struct col_str *col_str_from(char *str);

// Free memory of col_str
void free_col_str(struct col_str *str);

// Builder patterns for adding color and effect
struct col_str *with_color(struct col_str *str, enum colour_code col);
struct col_str *with_effect(struct col_str *str, enum effect_code eff,
			    int slot);

// Obtain raw representation
char *col_str_raw(struct col_str *str);

#endif // __COLOURED_H__
