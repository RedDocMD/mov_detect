#include "list.h"
#include <stdlib.h>

void ptr_list_init(struct ptr_list *list)
{
	list->arr = malloc(sizeof(*list->arr));
	list->cap = 1;
	list->sz = 0;
}

void ptr_list_free(struct ptr_list *list) { free(list->arr); }

void ptr_list_reserve(struct ptr_list *list, size_t new_cap)
{
	if (new_cap > list->cap) {
		list->arr = realloc(list->arr, new_cap * sizeof(*list->arr));
		list->cap = new_cap;
	}
}

void ptr_list_push(struct ptr_list *list, void *ptr)
{
	if (list->sz == list->cap)
		ptr_list_reserve(list, list->cap * 2);
	list->arr[(list->sz)++] = (uintptr_t)ptr;
}
