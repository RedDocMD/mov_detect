#include "coloured.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

struct col_str *col_str_from(char *str)
{
	struct col_str *col_str = malloc(sizeof(*col_str));
	col_str->buf = malloc(strlen(str));
	strcpy(col_str->buf, str);
	col_str->col = COL_DEFAULT;
	col_str->eff[0] = col_str->eff[1] = EF_NONE;
	return col_str;
}

void free_col_str(struct col_str *str)
{
	free(str->buf);
	free(str);
}

struct col_str *with_color(struct col_str *str, enum colour_code col)
{
	str->col = col;
	return str;
}

struct col_str *with_effect(struct col_str *str, enum effect_code eff, int slot)
{
	if (slot < 0 || slot >= 2) {
		char msg[128];
		sprintf(msg, "invalid slot: %d\n", slot);
		perror(msg);
		exit(1);
	}
	str->eff[slot] = eff;
	return str;
}

// str must point to where num must be appended.
// Returns to the end of the string
char *append_number(char *str, int num)
{
	int d, rev;

	rev = 0;
	while (num) {
		d = num % 10;
		rev = rev * 10 + d;
		num /= 10;
	}

	while (rev) {
		d = rev % 10;
		*(str++) = d + '0';
		rev /= 10;
	}
	*str = '\0';
	return str;
}

char *col_str_raw(struct col_str *str)
{
	int len, last, some;
	char *raw, *ret;

	len = strlen(str->buf) + 30;
	raw = malloc(len);
	ret = raw;
	memset(raw, 0, len);

	some = 0;
	if (str->col != COL_DEFAULT || str->eff[0] != EF_NONE ||
	    str->eff[1] != EF_NONE) {
		strcat(raw, "\033[");
		raw += 2;
		some = 1;
	}
	last = 0;
	if (str->col != COL_DEFAULT) {
		last = 1;
		raw = append_number(raw, str->col);
	}
	if (str->eff[0] != EF_NONE) {
		if (last)
			*(raw++) = ';';
		raw = append_number(raw, str->eff[0]);
	}
	if (str->eff[1] != EF_NONE) {
		if (last)
			*(raw++) = ';';
		raw = append_number(raw, str->eff[1]);
	}
	if (some)
		*(raw++) = 'm';
	strcat(ret, str->buf);
	if (some)
		strcat(ret, "\033[0m");

	return ret;
}
